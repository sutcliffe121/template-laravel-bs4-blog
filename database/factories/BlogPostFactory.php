<?php

namespace Database\Factories;

use App\Models\BlogPost;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Carbon\Carbon;

class BlogPostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BlogPost::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence;
        
        $body = collect($this->faker->paragraphs(rand(5, 15)))
        ->map(function($item){
            return "<p>$item</p>";
        })->toArray();
        $body = implode($body);

        return [
            'title' => $title,
            'blurb' => $this->faker->paragraph,
            'slug' => Str::slug($title),
            'body' => $body,
            'featured' => $this->faker->boolean(),
            'blog_category_id' => \App\Models\BlogCategory::pluck('id')->random(),
            'author' => \App\Models\User::pluck('id')->random(),
            'publish_at' => $this->faker->dateTime(),
            'image' => $this->faker->imageUrl(286, 180, 'planes', true),
            'state' => 'Published',
        ];

    }
}


 
