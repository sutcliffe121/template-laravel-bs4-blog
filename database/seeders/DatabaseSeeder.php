<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();
        \App\Models\BlogCategory::factory(5)->create();
        \App\Models\BlogTag::factory(10)->create();
        \App\Models\BlogPost::factory(10)->create();

        foreach(\App\Models\BlogPost::all() as $post){ // loop through all posts 
            $random_tags = \App\Models\BlogTag::all()->random(rand(1, 10))->pluck('id')->toArray();
            // Insert random post tag
            foreach ($random_tags as $tag) {
                DB::table('blog_post_blog_tag')->insert([
                    'blog_post_id' => $post->id,
                    'blog_tag_id' => \App\Models\BlogTag::all()->random(1)[0]->id
                ]);
            }
        }
    }
}
