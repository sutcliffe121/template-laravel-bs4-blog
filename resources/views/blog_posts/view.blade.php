@extends('layouts.app')

@section('page_title', $post->title)
@section('description', $post->blurb)

@push('meta')
    <meta property="og:type" content="article" />
    <meta property="article:section" content="Blog" />
    <meta property="og:title" content="{{ $post->title }}">
    <meta property="og:description" content="{{ $post->blurb }}">
    @isset($post->image)
      <meta property="og:image" content="{{ $post->image }}">
    @endisset
    <meta property="og:url" content="{{ Request::fullUrl() }}">
    <meta name="twitter:card" content="summary_large_image">
    <meta property="og:site_name" content="{{ config('app.name') }}">
    <meta name="twitter:image:alt" content="{{ $post->blurb }}">    
@endpush

@section('content')

<div class="row">

    <div class="col-md-8">
        <h1>{{ $post->title }}</h1>
        <p class="mb-4">Posted on {{ $post->publish_at->toFormattedDateString() }} by <a href="{{ route('blog.user', ['author' => $post->author]) }}">{{ $post->user->name }}</a> in <a href="{{ route('blog.categories', ['category' => $post->category->slug]) }}">{{ $post->category->title }}</a></p>

        <img class="img-fluid rounded float-left mr-3" src="{{ $post->image }}" alt="{{ $post->title }}" />
        
        {!! $post->body !!}

        <a onclick="history.back();" class="btn btn-primary">Go Back</a>
    </div>

    <div class="col-md-4">
        
        <!-- Share component -->
        <h3>Share</h3>
        <div class="card mb-4">
          <div class="card-body">
            @include('components.share')            
          </div>
        </div>

        <!-- Display tags -->
        @if($post->tags->isNotEmpty())
            <h3>Post Tags</h3>
            <div class="card mb-4">
              <div class="card-body">
                @each('blog_tags.list', $post->tags, 'tag')            
              </div>
            </div>
        @endif

        <!-- Display categories -->
        @if($categories->isNotEmpty())
            <h3>Categories</h3>
            <ul class="list-group">
                @each('blog_category.list', $categories, 'category')
            </ul>
        @endif

    </div>
</div>

@endsection