@extends('layouts.app')

@if(!empty($page_header))
  @section('page_title', $page_header)
@else
  @section('page_title', 'Posts')
@endif

@if(!empty($page_description))
  @section('description', $page_description)
@else
  @section('description', 'A Laravel blog page example made using Bootstrap 4.')
@endif

@section('content')
<div class="row">
    <div class="col-md-8">

        <!-- Display page header  -->
        @isset($page_header)
            <h1 class="mb-4">{{ $page_header }}</h1>
        @endisset

        <!-- Display posts  -->
        <div class="row">
            @each('blog_posts.list', $posts, 'post', 'blog_posts.empty')
        </div>

        <!-- Display pagination  -->
        {{ $posts->links() }}

    </div>
    <div class="col-md-4">

        <!-- Display categories -->
        @if($categories->isNotEmpty())
            <h3>Categories</h3>
            <ul class="list-group mb-4">
                @each('blog_category.list', $categories, 'category')
            </ul>
        @endif

        <!-- Display tags -->
        @if($tags->isNotEmpty())
            <h3>Tags</h3>
            <div class="card mb-4">
              <div class="card-body">
                @each('blog_tags.list', $tags, 'tag')            
              </div>
            </div>
        @endif

        <!-- Display featured posts -->
        @if($featuredPosts->isNotEmpty())
            <h3>Featured Posts</h3>
            <div class="card mb-4">
                <div class="card-body">   
                    @foreach($featuredPosts as $post)    
                    <div class="media @if(! $loop->last) mb-3 @endif">
                        <a href="{{ route('blog.view', ['slug' => $post->slug]) }}">
                            <img src="{{ $post->image }}" width="100" height="" class="mr-3 img-fluid" alt="{{ $post->title }}">
                        </a>
                        <div class="media-body">
                            <p class="text-muted p-0 m-0">{{ $post->publish_at->toFormattedDateString() }}</p>
                            <h5 class="mt-0"><a href="{{ route('blog.view', ['slug' => $post->slug]) }}">{{ $post->title }}</a></h5>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        @endif

        <!-- Display recent posts -->
        @if($recentPosts->isNotEmpty())
            <h3>Latest Posts</h3>
            <div class="card">
                <div class="card-body">   
                    @foreach($recentPosts as $post)    
                    <div class="media @if(! $loop->last) mb-3 @endif">
                        <a href="{{ route('blog.view', ['slug' => $post->slug]) }}">
                            <img src="{{ $post->image }}" width="100" height="" class="mr-3 img-fluid" alt="{{ $post->title }}">
                        </a>
                        <div class="media-body">
                            <p class="text-muted p-0 m-0">{{ $post->publish_at->toFormattedDateString() }}</p>
                            <h5 class="mt-0"><a href="{{ route('blog.view', ['slug' => $post->slug]) }}">{{ $post->title }}</a></h5>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        @endif

    </div>
</div>
@endsection
