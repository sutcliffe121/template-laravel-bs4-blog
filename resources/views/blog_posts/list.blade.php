<div class="col-md-6 d-flex align-items-stretch mb-4">
    <div class="card shadow-sm">
        <img class="card-img-top" src="{{ $post->image }}" alt="{{ $post->title }}">
        <div class="card-body">
            <h5 class="card-title">{{ $post->title }}</h5>
            <p class="card-text text-muted">{{ $post->publish_at->toFormattedDateString() }} by <a href="{{ route('blog.user', ['author' => $post->author]) }}">{{ $post->user->name }}</a></p>
            <p class="card-text">{{ $post->shortBlurb }}</p>
            <a href="{{ route('blog.view', ['slug' => $post->slug]) }}" class="btn btn-primary">Read more</a>
        </div>
    </div>
</div>
