<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BlogPost;

class BlogPostController extends Controller
{

    public function index()
    {
        $posts = BlogPost::published()->paginate(6);

        return view('blog_posts.posts', [
            'posts' => $posts,
        ]);
    }

    public function show($slug)
    {
        $post = BlogPost::where('slug', $slug)->published()->firstOrFail();

        return view('blog_posts.view', [
            'post' => $post,
        ]);
    }
    
}
