<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BlogTag;

class BlogTagController extends Controller
{
    
    public function index(BlogTag $tag)
    {
        $posts = $tag->posts()->published()->paginate(6);
        $page_header = 'Posts tagged: '.$tag->name.'';

        return view('blog_posts.posts', [
            'posts' => $posts,
            'page_header' => $page_header,
        ]);
    }
    
}
