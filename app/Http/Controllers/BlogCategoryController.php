<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BlogCategory;

class BlogCategoryController extends Controller
{
    
    public function index(BlogCategory $category)
    {
        $posts = $category->posts()->published()->paginate(6);
        $page_header = ''.$category->title.':';
        $page_description = $category->description;

        return view('blog_posts.posts', [
            'posts' => $posts,
            'page_header' => $page_header,
            'page_description' => $page_description
        ]);
    }

}
