<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BlogPost;
use App\Models\User;

class BlogPostUserController extends Controller
{
    public function index(User $author)
    {
        $posts = $author->posts()->published()->paginate(6);
        $page_header = 'Posts by '.$author->name.':';

        return view('blog_posts.posts', [
            'posts' => $posts,
            'page_header' => $page_header,
        ]);
    }  
}
