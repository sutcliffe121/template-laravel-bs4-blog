<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Pagination\Paginator;
use App\Models\BlogCategory;
use App\Models\BlogTag;
use App\Models\BlogPost;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

        // App is not running in CLI context
        // Do HTTP-specific stuff here
        if(! $this->app->runningInConsole()) {
            View::share('categories', BlogCategory::all());
            View::share('tags', BlogTag::has('posts')->get());
            View::share('recentPosts', BlogPost::published()->take(3)->get());
            View::share('featuredPosts', BlogPost::featured()->published()->take(3)->get());
        }   
    }
}
