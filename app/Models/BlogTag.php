<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogTag extends Model
{
    use HasFactory;

    /**
     * Get the posts which belong to the tag
     */    
    public function posts()
    {
        return $this->belongsToMany(BlogPost::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
        
}
