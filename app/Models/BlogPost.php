<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Carbon\Carbon;

class BlogPost extends Model
{
    use HasFactory;

    /**
     * The attributes that should be casted to native type by Eloquent.
     *
     * @var array
     */
    protected $casts = [
            'publish_at' => 'datetime',
    ];

    /**
     * Query scope - published posts
     */
    public function scopePublished($query)
    {
        $now = Carbon::now();

        return $query->where('state', 'Published')->where('publish_at', '<=', $now)->latest('publish_at');
    }

    /**
     * Query scope - published posts
     */
    public function scopeFeatured($query)
    {
        $now = Carbon::now();

        return $query->where('featured', 1);
    }

    /**
     * Get the user who made the post
     */
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'author', 'id');
    }

    /**
     * Get the tags which belong to the post
     */
    public function tags()
    {
        return $this->belongsToMany(BlogTag::class);
    }

    /**
     * Get the category that the post belongs to
     */
    public function category()
    {
        return $this->belongsTo(BlogCategory::class, 'blog_category_id', 'id');
    }

    /**
     * Limit blurb
     */
    public function getShortBlurbAttribute()
    {
        return Str::limit($this->blurb, 140, '...');
    }

}
