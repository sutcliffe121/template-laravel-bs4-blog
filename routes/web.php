<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


//Blog
Route::get('/', [App\Http\Controllers\BlogPostController::class, 'index'])->name('blog.posts');
Route::get('/blog/{slug}', [App\Http\Controllers\BlogPostController::class, 'show'])->name('blog.view');
Route::get('/blog/tags/{tag}', [App\Http\Controllers\BlogTagController::class, 'index'])->name('blog.tags');
Route::get('/blog/category/{category}', [App\Http\Controllers\BlogCategoryController::class, 'index'])->name('blog.categories');
Route::get('/blog/user/{author}', [App\Http\Controllers\BlogPostUserController::class, 'index'])->name('blog.user');
